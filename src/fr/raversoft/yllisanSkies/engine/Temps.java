package fr.raversoft.yllisanSkies.engine;

/**
 * Gestion du temps
 *
 * @author JALOUZET Jérémie
 */
public class Temps {

    private int secondes = 0;
    private int minutes = 0;
    private int heures = 0;
    private int delai = 1000;
    private long lastUpdate = System.currentTimeMillis();

    public int getSecondes() {
        return secondes;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getHeures() {
        return heures;
    }

    public void tick() {
        long tempsActuel = System.currentTimeMillis();
        if (tempsActuel - lastUpdate >= delai) {
            secondes++;
            lastUpdate = tempsActuel;
            if (secondes >= 60) {
                secondes = 0;
                minutes++;
                if (minutes >= 60) {
                    minutes = 0;
                    heures++;
                }
            }
        }
    }
}
