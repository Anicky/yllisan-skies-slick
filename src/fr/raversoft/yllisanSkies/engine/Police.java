package fr.raversoft.yllisanSkies.engine;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.font.effects.OutlineEffect;

/**
 * Polices utilisées dans le jeu
 *
 * @author JALOUZET Jérémie
 */
public class Police {

    private static Police instance = null;
    public final static int STANDARD = 0;
    private ArrayList<UnicodeFont> polices = null;

    private Police() throws SlickException {
        polices = new ArrayList<>();
        // Police normale
        Font font = new Font("Tahoma", Font.BOLD, 18);
        UnicodeFont police = new UnicodeFont(font, font.getSize(), font.isBold(), font.isItalic());
        //police.getEffects().add(new ColorEffect(Color.BLACK));
        //police.getEffects().add(new ShadowEffect(Color.WHITE, 1, 1, 1));
        police.getEffects().add(new ColorEffect(Color.WHITE));
        police.getEffects().add(new OutlineEffect(1, Color.BLACK));
        police.addAsciiGlyphs();
        police.loadGlyphs();
        polices.add(police);
    }

    private static Police getInstance() throws SlickException {
        if (instance == null) {
            instance = new Police();
        }
        return instance;
    }

    public static UnicodeFont getPolice(int i) throws SlickException {
        return getInstance().polices.get(i);
    }
}
