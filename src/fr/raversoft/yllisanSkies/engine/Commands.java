package fr.raversoft.yllisanSkies.engine;

import org.newdawn.slick.Input;

/**
 *
 * @author JALOUZET Jérémie
 */
public class Commands {

    private static int UP = Input.KEY_UP;
    private static int LEFT = Input.KEY_LEFT;
    private static int RIGHT = Input.KEY_RIGHT;
    private static int DOWN = Input.KEY_DOWN;
    private static int OK = Input.KEY_ENTER;
    private static int CANCEL = Input.KEY_NUMPAD0;

    public static int ok() {
        return OK;
    }

    public static void setOK(int VALIDER) {
        Commands.OK = VALIDER;
    }

    public static int cancel() {
        return CANCEL;
    }

    public static void setCANCEL(int ANNULER) {
        Commands.CANCEL = ANNULER;
    }

    public static int up() {
        return UP;
    }

    public static void setUP(int FLECHE_HAUT) {
        Commands.UP = FLECHE_HAUT;
    }

    public static int left() {
        return LEFT;
    }

    public static void setLEFT(int FLECHE_GAUCHE) {
        Commands.LEFT = FLECHE_GAUCHE;
    }

    public static int right() {
        return RIGHT;
    }

    public static void setRIGHT(int FLECHE_DROITE) {
        Commands.RIGHT = FLECHE_DROITE;
    }

    public static int down() {
        return DOWN;
    }

    public static void setDOWN(int FLECHE_BAS) {
        Commands.DOWN = FLECHE_BAS;
    }
}
