package fr.raversoft.yllisanSkies.engine;

import fr.raversoft.yllisanSkies.tests.Jeu;
import fr.raversoft.yllisanSkies.data.Hero;
import fr.raversoft.yllisanSkies.data.Sounds;
import org.newdawn.slick.*;

/**
 * Menu du jeu
 *
 * @author Jeremie
 */
public class Menu {

    private Jeu jeu = null;
    public boolean activable = true;
    public boolean active = false;
    private int selection = 1;
    private ImageMovement mouvement = null;
    private boolean apparition = false;
    private boolean disparition = false;
    private boolean deplacementCurseur = false;
    private int selectionAvant = 0;
    private final int ITEM_INVENTAIRE = 1;
    private final int ITEM_STATUT = 2;
    private final int ITEM_EQUIPEMENT = 3;
    private final int ITEM_CAPACITES = 4;
    private final int ITEM_AIRSHIP = 5;
    private final int ITEM_JOURNAL = 6;
    private final int ITEM_OPTIONS = 7;
    private final int ITEM_SAUVEGARDER = 8;
    private final int ITEM_QUITTER = 9;

    /**
     * Constructeur du menu
     *
     * @param activable Indque si le menu est activable
     */
    public Menu(Jeu jeu, boolean activable) {
        this.jeu = jeu;
        this.activable = activable;
    }

    public boolean estActivable() {
        return activable;
    }

    public boolean estActive() {
        return active;
    }

    public void setActivable(boolean activable) {
        this.activable = activable;
    }

    public void afficheInterface(Graphics g) throws SlickException {
        if (apparition) {
            Image noir = new Image("ressources/pictures/noir.png");
            float i = mouvement.getPosition();
            noir.setAlpha(i);
            g.drawImage(noir, 0, 0);
            mouvement.next();
            if (mouvement.isOver()) {
                apparition = false;
            }
        } else if (disparition) {
            Image noir = new Image("ressources/pictures/noir.png");
            float i = 0.5f - mouvement.getPosition();
            noir.setAlpha(i);
            g.drawImage(noir, 0, 0);
            mouvement.next();
            if (mouvement.isOver()) {
                disparition = false;
                active = false;
            }
        } else {
            Image noir = new Image("ressources/pictures/noir.png");
            noir.setAlpha(0.5f);
            g.drawImage(noir, 0, 0);
            for (int i = 1; i <= 9; i++) {
                afficheItem(g, i);
            }
            if (deplacementCurseur) {
                mouvement.next();
                if (mouvement.isOver()) {
                    deplacementCurseur = false;
                }
            }
            for (int i = 1; i <= jeu.getEquipe().getSize(); i++) {
                affichePerso(g, i, jeu.getEquipe().getPersonnage(i));
            }
            // Argent
            afficheArgent(g);

            // Temps
            afficheTemps(g);

            // Lieu
            afficheLieu(g);
        }
    }

    private void affichePerso(Graphics g, int position, Hero personnage) throws SlickException {
        int x = 0;
        if (position % 2 == 0) {
            x = Jeu.CASE;
        }
        float pourcentagePv = ((float) personnage.getPv() / personnage.getPvMax()) * 100;
        float barrePv = ((pourcentagePv * 180) / 100);
        float pourcentagePa = ((float) personnage.getPa() / personnage.getPaMax()) * 100;
        float barrePa = ((pourcentagePa * 180) / 100);
        g.drawImage(Resources.getPicture("item_perso"), 2 * Jeu.CASE - x, (1 + (position - 1) * 3) * Jeu.CASE);
        g.drawImage(Resources.getPicture(personnage.getImage()), 16 + 2 * Jeu.CASE - x, (1 + (position - 1) * 3) * Jeu.CASE);
        g.drawImage(Resources.getPicture("barrePerso"), 5 * Jeu.CASE + (Jeu.CASE / 2) - x, (2 + (position - 1) * 3) * Jeu.CASE);
        g.drawImage(Resources.getPicture("barrePerso"), 5 * Jeu.CASE + (Jeu.CASE / 2) - x, (3 + (position - 1) * 3) * Jeu.CASE);
        g.fillRect(5 * Jeu.CASE + (Jeu.CASE / 2) - x + 6, (2 + (position - 1) * 3) * Jeu.CASE + 6, barrePv, 2, Resources.getPicture("barrePv"), 0, 0);
        g.fillRect(5 * Jeu.CASE + (Jeu.CASE / 2) - x + 6, (3 + (position - 1) * 3) * Jeu.CASE + 6, barrePa, 2, Resources.getPicture("barrePa"), 0, 0);
        int positionNom = ((3 * Jeu.CASE) - Police.getPolice(Police.STANDARD).getWidth(personnage.toString())) / 2;
        Police.getPolice(Police.STANDARD).drawString((2 * Jeu.CASE) + positionNom - x, (3 + (position - 1) * 3) * Jeu.CASE, personnage.toString());
        Police.getPolice(Police.STANDARD).drawString(24 + 5 * Jeu.CASE - x, (1 + (position - 1) * 3) * Jeu.CASE + 6, jeu.getText("Caracteristiques.Nv") + " " + personnage.getNiveau());
        Police.getPolice(Police.STANDARD).drawString(24 + 5 * Jeu.CASE - x, (2 + (position - 1) * 3) * Jeu.CASE + 6, jeu.getText("Caracteristiques.Pv") + " " + personnage.getPv() + " / " + personnage.getPvMax());
        Police.getPolice(Police.STANDARD).drawString(24 + 5 * Jeu.CASE - x, (3 + (position - 1) * 3) * Jeu.CASE + 6, jeu.getText("Caracteristiques.Pa") + " " + personnage.getPa() + " / " + personnage.getPaMax());
    }

    private void afficheItem(Graphics g, int i) throws SlickException {
        String texte = "";
        String ref = "";
        int j = 0;
        switch (i) {
            case ITEM_INVENTAIRE:
                texte = jeu.getText("Menu.Inventaire");
                ref = "inventaire";
                break;
            case ITEM_STATUT:
                texte = jeu.getText("Menu.Statut");
                ref = "statut";
                j = 2;
                break;
            case ITEM_EQUIPEMENT:
                texte = jeu.getText("Menu.Equipement");
                ref = "equipement";
                j = 4;
                break;
            case ITEM_CAPACITES:
                texte = jeu.getText("Menu.Capacites");
                ref = "capacites";
                j = 6;
                break;
            case ITEM_AIRSHIP:
                texte = jeu.getText("Menu.Airship");
                ref = "airship";
                j = 8;
                break;
            case ITEM_JOURNAL:
                texte = jeu.getText("Menu.Journal");
                ref = "journal";
                j = 6;
                break;
            case ITEM_OPTIONS:
                texte = jeu.getText("Menu.Options");
                ref = "options";
                j = 4;
                break;
            case ITEM_SAUVEGARDER:
                texte = jeu.getText("Menu.Sauvegarder");
                ref = "sauvegarder";
                j = 2;
                break;
            case ITEM_QUITTER:
                texte = jeu.getText("Menu.Quitter");
                ref = "quitter";
                break;
        }
        float k = 0;
        if (deplacementCurseur) {
            if (i == selection) {
                k = mouvement.getPosition();
            } else if (i == selectionAvant) {
                k = Jeu.CASE - mouvement.getPosition();
            }
        } else if (selection == i) {
            k = Jeu.CASE;
        }
        g.drawImage(Resources.getPicture("item_" + ref), 14 * Jeu.CASE - j - k, i * Jeu.CASE);
        Police.getPolice(Police.STANDARD).drawString(15 * Jeu.CASE + 4 - j - k, i * Jeu.CASE + 4, texte);
    }

    private void afficheArgent(Graphics g) throws SlickException {
        String argent = jeu.getEquipe().getArgent() + " " + jeu.getText("Systeme.Monnaie");
        int argentPosition = Police.getPolice(Police.STANDARD).getWidth(argent);

        g.drawImage(Resources.getPicture("item_argent"), 13 * Jeu.CASE, 11 * Jeu.CASE);
        Police.getPolice(Police.STANDARD).drawString(18 * Jeu.CASE - argentPosition + 8, 4 + 11 * Jeu.CASE, argent);
    }

    private void afficheTemps(Graphics g) throws SlickException {
        g.drawImage(Resources.getPicture("item_temps"), 13 * Jeu.CASE, 12 * Jeu.CASE);
        int heures = jeu.getTemps().getHeures();
        int minutes = jeu.getTemps().getMinutes();
        int secondes = jeu.getTemps().getSecondes();

        String temps = "";
        if (heures < 10) {
            temps += "0";
        }
        temps += heures + ":";
        if (minutes < 10) {
            temps += "0";
        }
        temps += minutes + ":";
        if (secondes < 10) {
            temps += "0";
        }
        temps += secondes;
        int tempsPosition = Police.getPolice(Police.STANDARD).getWidth(temps);
        Police.getPolice(Police.STANDARD).drawString(18 * Jeu.CASE - tempsPosition + 8, 4 + 12 * Jeu.CASE, temps);
    }

    private void afficheLieu(Graphics g) throws SlickException {
        String lieu = jeu.getText("Place.HopeForest");
        int lieuPosition = (Jeu.ECRAN_LARGEUR - Police.getPolice(Police.STANDARD).getWidth(lieu)) / 2;

        g.drawImage(Resources.getPicture("item_lieu"), 0, 14 * Jeu.CASE);
        Police.getPolice(Police.STANDARD).drawString(lieuPosition, 14 * Jeu.CASE + 4, lieu);
    }

    private void disparition() {
        disparition = true;
        mouvement = new ImageMovement(10, 0.5f);
    }

    public void apparition() {
        active = true;
        selection = 1;
        apparition = true;
        mouvement = new ImageMovement(10, 0.5f);
    }

    private void deplacementCurseur() {
        deplacementCurseur = true;
        mouvement = new ImageMovement(10, 32);
    }

    private void validation() {
        switch (selection) {
            case ITEM_INVENTAIRE:
                jeu.getSons().play(Sounds.BUZZER);
                break;
            case ITEM_STATUT:
                jeu.getSons().play(Sounds.BUZZER);
                break;
            case ITEM_EQUIPEMENT:
                jeu.getSons().play(Sounds.BUZZER);
                break;
            case ITEM_CAPACITES:
                jeu.getSons().play(Sounds.BUZZER);
                break;
            case ITEM_AIRSHIP:
                jeu.getSons().play(Sounds.BUZZER);
                break;
            case ITEM_JOURNAL:
                jeu.getSons().play(Sounds.BUZZER);
                break;
            case ITEM_OPTIONS:
                jeu.getSons().play(Sounds.BUZZER);
                break;
            case ITEM_SAUVEGARDER:
                jeu.getSons().play(Sounds.BUZZER);
                break;
            case ITEM_QUITTER:
                jeu.getSons().play(Sounds.OK);
                jeu.quitter();
                break;
        }
    }

    public void gestionClavier(GameContainer container) throws SlickException {
        if (!apparition && !disparition) {
            if (container.getInput().isKeyPressed(Commands.ok())) {
                validation();
            }
            if (container.getInput().isKeyPressed(Commands.cancel())) {
                disparition();
                jeu.getSons().play(Sounds.CANCEL);
            }

            if (!deplacementCurseur) {
                // Touche Haut
                if (container.getInput().isKeyDown(Commands.up())) {
                    jeu.getSons().play(Sounds.CURSOR);
                    selectionAvant = selection;
                    selection--;
                    if (selection <= 0) {
                        selection = 9;
                    }
                    deplacementCurseur();
                } // Touche Bas
                if (container.getInput().isKeyDown(Commands.down())) {
                    jeu.getSons().play(Sounds.CURSOR);
                    selectionAvant = selection;
                    selection++;
                    if (selection >= 10) {
                        selection = 1;
                    }
                    deplacementCurseur();
                }
            }
        }
    }
}
