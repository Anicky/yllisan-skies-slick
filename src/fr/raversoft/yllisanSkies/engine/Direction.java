package fr.raversoft.yllisanSkies.engine;

/**
 * Direction prise par un personnage
 *
 * @author JALOUZET Jérémie
 */
public enum Direction {

    Bas, Gauche, Droite, Haut
}
