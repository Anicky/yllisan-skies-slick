/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.raversoft.yllisanSkies.engine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.util.ResourceLoader;

/**
 *
 * @author Jeremie
 */
public class Resources {

    private static String FILE_SEPARATOR = File.separator;

    private static String getResourceFolder() {
        return "fr" + FILE_SEPARATOR + "raversoft" + FILE_SEPARATOR + "yllisanSkies" + FILE_SEPARATOR + "res" + FILE_SEPARATOR;
    }

    public static Image getCharSet(String name) throws SlickException {
        return new Image(getResourceFolder() + "charset" + FILE_SEPARATOR + name + ".png");
    }

    public static Image getChipSet(String name) throws SlickException {
        return new Image(getResourceFolder() + "chipset" + FILE_SEPARATOR + name + ".png");
    }

    public static InputStream getData(String name) throws FileNotFoundException {
        return ResourceLoader.getResourceAsStream(getResourceFolder() + "database" + FILE_SEPARATOR + name + ".properties");
    }

    public static String getMap(String name) {
        return getResourceFolder() + "maps" + FILE_SEPARATOR + name + ".tmx";
    }

    public static Music getMusic(String name) throws SlickException {
        return new Music(getResourceFolder() + "music" + FILE_SEPARATOR + name + ".ogg");
    }

    public static Sound getSound(String name) throws SlickException {
        return new Sound(getResourceFolder() + "sounds" + FILE_SEPARATOR + name + ".wav");
    }

    public static Image getPanorama(String name) throws SlickException {
        return new Image(getResourceFolder() + "panoramas" + FILE_SEPARATOR + name + ".png");
    }

    public static Image getPicture(String name) throws SlickException {
        return new Image(getResourceFolder() + "pictures" + FILE_SEPARATOR + name + ".png");
    }

    public static InputStream getText(String name) throws FileNotFoundException {
        return ResourceLoader.getResourceAsStream(getResourceFolder() + "texts" + FILE_SEPARATOR + name + ".properties");
    }
}
