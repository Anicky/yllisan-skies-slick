package fr.raversoft.yllisanSkies.engine;

/**
 *
 * Gère le temps nécessaire pour déplacer une image
 *
 * @author JALOUZET Jérémie
 */
public class ImageMovement {

    private int frame_actual = 0;
    private int frame_number = 0;
    private float pixels_number = 0;
    private boolean inMovement = false;

    public ImageMovement(int nombreFrames, float nombrePixels) {
        this.frame_number = nombreFrames;
        this.pixels_number = nombrePixels;
        start();
    }

    /**
     * Starts the movement
     */
    private void start() {
        inMovement = true;
        frame_actual = 0;
    }

    /**
     * Passe à la frame suivante
     */
    public void next() {
        frame_actual++;
        if (frame_actual >= frame_number) {
            stop();
        }
    }

    /**
     * Récupère la position actuelle de l'image
     *
     * @return La position de l'image selon la frame actuelle
     */
    public float getPosition() {
        return (pixels_number / frame_number) * frame_actual;
    }

    /**
     * Stops the movement
     */
    private void stop() {
        inMovement = false;
    }

    /**
     * Returns true if
     *
     * @return Un booléen indiquant si le mouvement est fini
     */
    public boolean isOver() {
        return !inMovement;
    }
}
