package fr.raversoft.yllisanSkies.engine;

/**
 * Carte du jeu
 *
 * @author JALOUZET Jérémie
 */
import fr.raversoft.yllisanSkies.tests.Jeu;
import java.util.ArrayList;
import java.util.Map.Entry;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.GroupObject;
import org.newdawn.slick.tiled.ObjectGroup;
import org.newdawn.slick.tiled.TiledMapPlus;

public class Map {

    /**
     * Fichier contenant toutes les informations de la map
     */
    private static TiledMapPlus map;
    /**
     * Liste des blocs obstacles se trouvant sur la map
     */
    private static ArrayList<Bloc> blocs;
    /**
     * Coordonnée X pour l'affichage de la map
     */
    public int x = 0;
    /**
     * Coordonnée Y pour l'affichage de la map
     */
    public int y = 0;
    /**
     * Couche basse
     */
    public final static int COUCHE_BASSE = 0;
    /**
     * Couche intermédiaire
     */
    public final static int COUCHE_INTERMEDIAIRE = 1;
    /**
     * Couche haute
     */
    public final static int COUCHE_HAUTE = 2;
    /**
     * Couche panorama
     */
    public final static int COUCHE_PANORAMA = 3;
    /**
     * Couche collisions
     */
    public final static int COUCHE_COLLISIONS = 4;
    /**
     * Objets
     */
    ObjectGroup pnj = null;

    /**
     * Constructeur de la map
     *
     * @param name Le nom de la map (lien vers le fichier)
     * @throws SlickException
     */
    public Map(String name) throws SlickException {
        blocs = new ArrayList<>();
        map = new TiledMapPlus(name);
        pnj = map.getObjectGroup("PNJ");
        for (GroupObject e : pnj.getObjects()) {
            System.out.println(e.name + " (" + e.type + ") X : " + e.x + "Y :" + e.y);
            System.out.println("Index : " + e.index + " GID : " + e.gid + " Height : " + e.height + " Width : " + e.width);
            if (e.props != null) {
                for (Entry es : e.props.entrySet()) {
                    System.out.println(" - " + es.getKey() + " : " + es.getValue());
                }
            }
        }
        ajouteBlocs();
    }

    /**
     * Ajoute les blocs (obstacles) se trouvant sur la map
     */
    private void ajouteBlocs() {
        for (int bloc_x = 0; bloc_x < getLargeurTiles(); bloc_x++) {
            for (int bloc_y = 0; bloc_y < getHauteurTiles(); bloc_y++) {
                int tileID = map.getTileId(bloc_x, bloc_y, COUCHE_COLLISIONS);
                if (tileID != 0) {
                    blocs.add(new Bloc(bloc_x, bloc_y, Jeu.CASE));
                }
            }
        }
    }

    /**
     * Récupère la largeur de la map en tiles
     *
     * @return La largeur de la map en tiles
     */
    public int getLargeurTiles() {
        return map.getWidth();
    }

    /**
     * Récupère la hauteur de la map en tiles
     *
     * @return La hauteur de la map en tiles
     */
    public int getHauteurTiles() {
        return map.getHeight();
    }

    /**
     * Récupère la largeur de la map en pixels
     *
     * @return La largeur de la map en pixels
     */
    public int getLargeur() {
        return getLargeurTiles() * map.getTileWidth();
    }

    /**
     * Récupère la hauteur de la map en pixels
     *
     * @return La hauteur de la map en pixels
     */
    public int getHauteur() {
        return getHauteurTiles() * map.getTileHeight();
    }

    /**
     * Récupère le nombre de blocs (obstacles) de la map
     *
     * @return Le nombre de blocs (obstacles) de la map
     */
    public int getNombreBlocs() {
        return blocs.size();
    }

    /**
     * Récupère un bloc à partir de son indice
     *
     * @param i L'indice du bloc
     * @return Le bloc
     */
    public Bloc getBloc(int i) {
        return blocs.get(i);
    }

    /**
     * Affiche une couche de la map
     *
     * @param couche La couche à afficher
     */
    public void afficheCouche(final int couche) {
        map.render(x, y, couche);
    }

    /**
     * Met à jour les coordonnées des blocs sur la map
     */
    public void updateBlocs() {
        for (int i = 0; i < getNombreBlocs(); i++) {
            getBloc(i).updateForme(x, y);
        }
    }
}