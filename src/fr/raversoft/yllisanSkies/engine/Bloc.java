package fr.raversoft.yllisanSkies.engine;

/**
 *
 * @author JALOUZET Jérémie
 */
import fr.raversoft.yllisanSkies.tests.Jeu;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Bloc {

    /**
     * La forme associée au bloc
     */
    private Rectangle forme;
    /**
     * La coordonnée x (abscisse) du bloc sur la map
     */
    private int map_x = 0;
    /**
     * La coordonnée y (ordonnée) du bloc sur la map
     */
    private int map_y = 0;

    /**
     * Constructeur du bloc (pour un rectangle)
     *
     * @param x La coordonnée x (abscisse) du bloc sur la map
     * @param y La coordonnée y (ordonnée) du bloc sur la map
     * @param largeur La largeur du bloc
     * @param hauteur La hauteur du bloc
     */
    public Bloc(int x, int y, int largeur, int hauteur) {
        this.map_x = x;
        this.map_y = y;
        forme = new Rectangle(x, y, largeur - 2, hauteur - 2);
    }

    /**
     * Constructeur du bloc (pour un carré)
     *
     * @param x La coordoonée x (abscisse) du bloc sur la map
     * @param y La coordoonée y (ordonnée) du bloc sur la map
     * @param largeur La largeur (et aussi sa hauteur) du bloc
     */
    public Bloc(int x, int y, int largeur) {
        this.map_x = x;
        this.map_y = y;
        forme = new Rectangle(x, y, largeur - 2, largeur - 2);
    }

    /**
     * Affiche la forme (en mode débug)
     *
     * @param g Les graphismes sur lequel afficher la forme
     */
    public void afficheForme(Graphics g) {
        g.setColor(Color.blue);
        g.draw(forme);
    }

    /**
     * Récupère la forme du bloc
     *
     * @return La forme du bloc
     */
    public Shape getForme() {
        return forme;
    }

    /**
     * Récupère la coordonnée x (abscisse) du bloc sur la map
     *
     * @return La coordoonnée x (abscisse) du bloc sur la map
     */
    public int getMapX() {
        return map_x;
    }

    /**
     * Récupère la coordonnée y (ordonnée) du bloc sur la map
     *
     * @return La coordonnée y (ordonnée) du bloc sur la map
     */
    public int getMapY() {
        return map_y;
    }

    /**
     * Modifie les coordonnées de la forme
     *
     * @param x La coordonnée x
     * @param y La cooordonnée y
     */
    public void updateForme(int x, int y) {
        forme.setX((map_x * Jeu.CASE) + x + 1);
        forme.setY((map_y * Jeu.CASE) + y + 1);
    }
}
