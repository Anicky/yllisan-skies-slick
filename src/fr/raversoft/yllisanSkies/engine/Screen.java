package fr.raversoft.yllisanSkies.engine;

/**
 *
 * @author JALOUZET Jérémie
 */
public class Screen {

    private int largeur = 0;
    private int hauteur = 0;
    private boolean pleinEcran = false;
    private boolean modeDebug = false;
    private boolean modeFantome = false;

    public Screen(int largeur, int hauteur, boolean pleinEcran, boolean modeDebug) {
        this.largeur = largeur;
        this.hauteur = hauteur;
        this.pleinEcran = pleinEcran;
        this.modeDebug = modeDebug;
    }

    public int getLargeur() {
        return largeur;
    }

    public int getHauteur() {
        return hauteur;
    }

    public int getMilieuLargeur() {
        return (largeur / 2);
    }

    public int getMilieuHauteur() {
        return (hauteur / 2);
    }

    public boolean estModeDebug() {
        return modeDebug;
    }

    public boolean estPleinEcran() {
        return pleinEcran;
    }

    public void setPleinEcran(boolean b) {
        pleinEcran = b;
    }

    public void setModeDebug(boolean b) {
        modeDebug = b;
    }

    public boolean estModeFantome() {
        return modeFantome;
    }

    public void setModeFantome(boolean b) {
        modeFantome = b;
    }
}
