package fr.raversoft.yllisanSkies.tests;

import fr.raversoft.yllisanSkies.engine.Police;
import fr.raversoft.yllisanSkies.engine.Temps;
import fr.raversoft.yllisanSkies.engine.Screen;
import fr.raversoft.yllisanSkies.engine.Map;
import fr.raversoft.yllisanSkies.engine.Menu;
import fr.raversoft.yllisanSkies.engine.Direction;
import fr.raversoft.yllisanSkies.engine.Camera;
import fr.raversoft.yllisanSkies.engine.Commands;
import fr.raversoft.yllisanSkies.data.Party;
import fr.raversoft.yllisanSkies.data.Maps;
import fr.raversoft.yllisanSkies.data.MusicList;
import fr.raversoft.yllisanSkies.data.Options;
import fr.raversoft.yllisanSkies.data.Hero;
import fr.raversoft.yllisanSkies.data.Sounds;
import fr.raversoft.yllisanSkies.engine.Resources;
import fr.raversoft.yllisanSkies.exceptions.FullPartyException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.openal.AL;
import org.newdawn.slick.*;

public class Jeu extends BasicGame {

    private String langue = "fr";
    private Properties textes = null;
    private Camera camera = null;
    private Maps maps = null;
    private Screen ecran;
    private AppGameContainer gameContainer;
    private Party equipe = null;
    private Menu menu = null;
    private Temps temps = null;
    private Sounds sons = null;
    private MusicList musiques = null;
    private Map map_actuelle = null;
    // Caractéristiques affichage
    public final static String ECRAN_TITRE = "Yllisan Skies";
    public final static int ECRAN_LARGEUR = 640;
    public final static int ECRAN_HAUTEUR = 480;
    public final static int CASE = 32;
    public final static int ECRAN_FPS = 60;
    public final static boolean ECRAN_PLEIN_ECRAN = false;
    public final static boolean ECRAN_DEBUG = false;

    public Jeu() throws SlickException, FileNotFoundException, IOException {
        super(ECRAN_TITRE);
        equipe = new Party();
        ecran = new Screen(ECRAN_LARGEUR, ECRAN_HAUTEUR, ECRAN_PLEIN_ECRAN, ECRAN_DEBUG);
        gameContainer = new AppGameContainer(this, ecran.getLargeur(), ecran.getHauteur(), ecran.estPleinEcran());
        menu = new Menu(this, true);
        temps = new Temps();
        sons = new Sounds();
        musiques = new MusicList();
        textes = new Properties();
        try {
            Hero.creerHeros();
        } catch (IOException ex) {
            Logger.getLogger(Jeu.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            equipe.addHero(Hero.getHeros("Cyril"));
            equipe.addHero(Hero.getHeros("Max"));
            equipe.addHero(Hero.getHeros("Yuna"));
            equipe.addHero(Hero.getHeros("Leonard"));
        } catch (FullPartyException ex) {
            Logger.getLogger(Jeu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getText(String s) {
        return textes.getProperty(s);
    }

    public Sounds getSons() {
        return sons;
    }

    @Override
    public void init(GameContainer container) throws SlickException {
        container.setVSync(true);
        container.setTargetFrameRate(ECRAN_FPS);
        container.setShowFPS(false);
        maps = new Maps();
        try {
            textes.load(Resources.getText(langue));
        } catch (IOException ex) {
            Logger.getLogger(Jeu.class.getName()).log(Level.SEVERE, null, ex);
        }
        map_actuelle = maps.getMap(Maps.FORET_DES_ESPOIRS_1);
        //camera = new Camera(this, ecran, map_actuelle, 2 * 32, 20 * 32, Direction.Droite);
        gameContainer.setDefaultFont(Police.getPolice(Police.STANDARD));
        afficherCommandes();
        musiques.play(MusicList.HOPE_FOREST, 0.5f);

    }

    public void start() throws SlickException {
        gameContainer.start();
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        temps.tick();
        gestionClavier(container);
        if (menu.estActive()) {
            menu.gestionClavier(container);
        } else {
            if (!camera.estEnMouvement()) {
                gestionMenu(container);
                camera.gestionClavier(container);
            } else {
                camera.deplacement(camera.getDirection());
            }
        }
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        camera.affiche(g);
        if (Options.getEffetsLumiere()) {
            g.drawImage(new Image("ressources/pictures/effets_lumiere/Effet_Soleil_03.png"), 0, 0, new Color(1f, 1f, 1f, 0.4f));
        }
        if (menu.estActive()) {
            menu.afficheInterface(g);
        }
        if (ecran.estModeDebug()) {
            afficheDebug(g);
        }
        if (ecran.estModeFantome()) {
            afficheModeFantome();
        }
    }

    public Party getEquipe() {
        return equipe;
    }

    private void afficherCommandes() {
        System.out.println("Fleches Gauche, Haut, Droite, Bas --- Se diriger");
        System.out.println("Z --- Valider");
        System.out.println("X --- Annuler (Permet aussi d'entrer et sortir du menu)");
        System.out.println("F4 --- Activer/Désactiver le mode plein écran");
        System.out.println("F5 --- Activer/Désactiver le mode fantome (Le personnage peut passer à travers les obstacles)");
        System.out.println("F6 --- Activer/Désactiver le mode Débug (Affichage des FPS, d'une grille, et des obstacles)");
    }

    private void afficheInfosPositions() throws SlickException {
        String persoEcran = "Perso.Ecran X " + (camera.getPersoEcranX() + CASE / 4) + " Y " + (camera.getPersoEcranY() + CASE);
        String persoMapEcran = "Perso.Map X " + camera.getPersoMapX() + " Y " + camera.getPersoMapY();
        String mapEcran = "Map.Ecran X " + map_actuelle.x + " Y " + map_actuelle.y;
        Police.getPolice(Police.STANDARD).drawString(ECRAN_LARGEUR - Police.getPolice(Police.STANDARD).getWidth(persoEcran) - (CASE / 4), CASE / 4, persoEcran);
        Police.getPolice(Police.STANDARD).drawString(ECRAN_LARGEUR - Police.getPolice(Police.STANDARD).getWidth(persoMapEcran) - (CASE / 4), CASE / 4 + 1 * CASE, persoMapEcran);
        Police.getPolice(Police.STANDARD).drawString(ECRAN_LARGEUR - Police.getPolice(Police.STANDARD).getWidth(mapEcran) - (CASE / 4), CASE / 4 + 2 * CASE, mapEcran);
    }

    private void switchModeDebug() {
        if (ecran.estModeDebug()) {
            ecran.setModeDebug(false);
            gameContainer.setShowFPS(false);
        } else {
            ecran.setModeDebug(true);
            gameContainer.setShowFPS(true);
        }
    }

    private void switchModeFantome() {
        if (ecran.estModeFantome()) {
            ecran.setModeFantome(false);
        } else {
            ecran.setModeFantome(true);
        }
    }

    private void switchSetFullscreen() throws SlickException {
        if (ecran.estPleinEcran()) {
            ecran.setPleinEcran(false);
        } else {
            ecran.setPleinEcran(true);
        }
        gameContainer.setFullscreen(ecran.estPleinEcran());
    }

    public void quitter() {
        AL.destroy();
        gameContainer.destroy();
        System.exit(0);
    }

    private void gestionMenu(GameContainer container) {
        if (container.getInput().isKeyPressed(Commands.cancel())) {
            sons.play(Sounds.CANCEL);
            menu.apparition();
        }
    }

    private void gestionClavier(GameContainer container) throws SlickException {
        // Touche F4 (Fullscreen)
        if (container.getInput().isKeyPressed(Input.KEY_F4)) {
            switchSetFullscreen();
        }

        // Touche F5 (Fantome)
        if (container.getInput().isKeyPressed(Input.KEY_F5)) {
            switchModeFantome();
        }

        // Touche F6 (Debug)
        if (container.getInput().isKeyPressed(Input.KEY_F6)) {
            switchModeDebug();
        }
    }

    public Temps getTemps() {
        return temps;
    }

    private void afficheModeFantome() throws SlickException {
        Police.getPolice(Police.STANDARD).drawString(CASE / 4, 14 * CASE, "Mode fantôme");
    }

    private void afficheDebug(Graphics g) throws SlickException {
        afficheGrille(g);
        for (int i = 0; i < map_actuelle.getNombreBlocs(); i++) {
            map_actuelle.getBloc(i).afficheForme(g);
        }
        g.setColor(org.newdawn.slick.Color.red);
        g.draw(camera.getPersoContour());
        afficheInfosPositions();
    }

    private void afficheGrille(Graphics g) throws SlickException {
        int x = map_actuelle.x % CASE;
        int y = map_actuelle.y % CASE;
        g.drawImage(new Image("ressources/pictures/debug_grille.png"), x, y);
    }

    public static void main(String[] args) {
        try {
            new Jeu().start();
        } catch (SlickException | IOException ex) {
            Logger.getLogger(Jeu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
