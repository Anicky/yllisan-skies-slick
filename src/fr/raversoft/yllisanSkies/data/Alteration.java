package fr.raversoft.yllisanSkies.data;

/**
 * Altération of a hero / enemy.
 *
 * @author JALOUZET Jérémie
 */
public enum Alteration {

    Confusion, Fatigue, Mutism, Sleep, Paralysis, Poison, Blindness, KO
}
