package fr.raversoft.yllisanSkies.data;

import fr.raversoft.yllisanSkies.engine.Resources;
import java.util.ArrayList;
import java.util.List;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;

/**
 *
 * @author JALOUZET Jérémie
 */
public class MusicList {

    private List<Music> musiques = new ArrayList<>();
    public final static int TITLE_SCREEN = 0;
    public final static int HOPE_FOREST = 1;
    private float volume = 1.0f;
    private int music_actual = 0;
    
    public void setVolume(float volume) {
        this.volume = volume;
    }

    public MusicList() throws SlickException {
        musiques.add(Resources.getMusic("TitleScreen"));
        musiques.add(Resources.getMusic("HopeForest"));
    }

    public void play(int music) {
        play(music, 1.0f, 1.0f);
    }

    public void play(int music, float volume) {
        play(music, 1.0f, volume);
    }

    public void play(int music, float pitch, float volume) {
        musiques.get(music).play(pitch, (this.volume) * volume);
        music_actual = music;
    }
    
    public void fade(int millis) {
        musiques.get(music_actual).fade(millis, 0.0f, true);
    }
}
