package fr.raversoft.yllisanSkies.data;

import fr.raversoft.yllisanSkies.data.items.equipment.Accessory;
import fr.raversoft.yllisanSkies.data.items.equipment.Weapon;
import fr.raversoft.yllisanSkies.data.items.equipment.Protection;
import fr.raversoft.yllisanSkies.engine.Resources;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import org.newdawn.slick.SlickException;

/**
 * Personnage de l'équipe
 *
 * @author JALOUZET Jérémie
 */
public class Hero {

    private static ArrayList<Hero> liste = new ArrayList<>();
    /**
     * Prénom du personnage
     */
    private String prenom = "";
    /**
     * Nom du personnage
     */
    private String nom = "";
    /**
     * Altération d'état actuelle du personnage
     */
    private Alteration etat = null;
    /**
     * Image du personnage
     */
    private String image = "";
    // Caractéristiques
    /**
     * Niveau du personnage
     */
    private int niveau = 24;
    /**
     * Points de vie actuels du personnage
     */
    private int pv = 4567;
    /**
     * Points de vie maximum du personnage
     */
    private int pvMax = 8912;
    /**
     * Points d'action actuels du personnage
     */
    private int pa = 1432;
    /**
     * Points d'action maximum du personnage
     */
    private int paMax = 2507;
    /**
     * Points de capacité actuels du personnage
     */
    private int pc = 0;
    /**
     * Points de capacité maximum du personnage
     */
    private int pcMax = 0;
    /**
     * Expérience du personnage
     */
    private int experience = 0;
    // Attributs
    /**
     * Force du personnage
     */
    private int force = 0;
    /**
     * Résistance du personnage
     */
    private int resistance = 0;
    /**
     * Potentiel du personnage
     */
    private int potentiel = 0;
    /**
     * Esprit du personnage
     */
    private int esprit = 0;
    /**
     * Agilité du personnage
     */
    private int agilite = 0;
    // Eléments
    /**
     * Affiliation du personnage à l'élément Feu
     */
    private int element_feu = 0;
    /**
     * Affiliation du personnage à l'élément Air
     */
    private int element_air = 0;
    /**
     * Affiliation du personnage à l'élément Foudre
     */
    private int element_foudre = 0;
    /**
     * Affiliation du personnage à l'élément Lumière
     */
    private int element_lumiere = 0;
    /**
     * Affiliation du personnage à l'élément Eau
     */
    private int element_eau = 0;
    /**
     * Affiliation du personnage à l'élément Terre
     */
    private int element_terre = 0;
    /**
     * Affiliation du personnage à l'élément Nature
     */
    private int element_nature = 0;
    /**
     * Affiliation du personnage à l'élément Ténèbres
     */
    private int element_tenebres = 0;
    // Equipement
    /**
     * Arme actuelle du personnage
     */
    private Weapon arme;
    /**
     * Protection actuelle du personnage
     */
    private Protection protection = null;
    /**
     * Accessoire n°1 actuel du personnage
     */
    private Accessory accessoire1 = null;
    /**
     * Accessoire n°2 actuel du personnage
     */
    private Accessory accessoire2 = null;

    public static Hero getHeros(int i) {
        return liste.get(i);
    }

    public static Hero getHeros(String s) {
        Hero h = null;
        boolean trouve = false;
        Iterator<Hero> it = liste.iterator();
        while (!trouve && it.hasNext()) {
            Hero h1 = it.next();
            if (h1.getPrenom().equals(s)) {
                h = h1;
                trouve = true;
            }
        }
        return h;
    }

    public static void creerHeros() throws SlickException, IOException {
        Properties properties = new Properties();
        properties.load(Resources.getData("heroes"));
        int nombre = Integer.parseInt(properties.getProperty("nombre", "0"));
        for (int i = 1; i <= nombre; i++) {
            String prenom = properties.getProperty(i + ".prenom", "");
            String nom = properties.getProperty(i + ".nom", "");
            String image = Character.toLowerCase(prenom.charAt(0)) + (prenom.length() > 1 ? prenom.substring(1) : "");
            Hero h = new Hero(prenom, nom, image);
            h.setNiveau(Integer.parseInt(properties.getProperty(i + ".niveau", "1")));
            h.setPvMax(Integer.parseInt(properties.getProperty(i + ".pv", "1")));
            h.setPaMax(Integer.parseInt(properties.getProperty(i + ".pa", "1")));
            h.setPcMax(Integer.parseInt(properties.getProperty(i + ".pc", "100")));
            h.setForce(Integer.parseInt(properties.getProperty(i + ".force", "1")));
            h.setResistance(Integer.parseInt(properties.getProperty(i + ".resistance", "1")));
            h.setPotentiel(Integer.parseInt(properties.getProperty(i + ".potentiel", "1")));
            h.setEsprit(Integer.parseInt(properties.getProperty(i + ".esprit", "1")));
            h.setPv(h.pvMax);
            h.setPa(h.paMax);
            liste.add(h);
        }
    }

    /**
     * Constructeur du personnage
     *
     * @param prenom Prénom du personnage
     * @param nom Nom du personnage
     * @param image Image du personnage
     */
    public Hero(String prenom, String nom, String image) {
        this.prenom = prenom;
        this.nom = nom;
        this.image = image;
    }

    /**
     * Récupère l'image du personnage
     *
     * @return L'image du personnage
     */
    public String getImage() {
        return image;
    }

    /**
     * Affiche le prénom du personnage
     *
     * @return Le prénom du personnage
     */
    @Override
    public String toString() {
        return prenom;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public int getPv() {
        return pv;
    }

    public void setPv(int pv) {
        this.pv = pv;
    }

    public int getPvMax() {
        return pvMax;
    }

    public void setPvMax(int pvMax) {
        this.pvMax = pvMax;
    }

    public int getPa() {
        return pa;
    }

    public void setPa(int pa) {
        this.pa = pa;
    }

    public int getPaMax() {
        return paMax;
    }

    public void setPaMax(int paMax) {
        this.paMax = paMax;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Alteration getEtat() {
        return etat;
    }

    public void setEtat(Alteration etat) {
        this.etat = etat;
    }

    public int getPc() {
        return pc;
    }

    public void setPc(int pc) {
        this.pc = pc;
    }

    public int getPcMax() {
        return pcMax;
    }

    public void setPcMax(int pcMax) {
        this.pcMax = pcMax;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getForce() {
        return force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public int getResistance() {
        return resistance;
    }

    public void setResistance(int resistance) {
        this.resistance = resistance;
    }

    public int getPotentiel() {
        return potentiel;
    }

    public void setPotentiel(int potentiel) {
        this.potentiel = potentiel;
    }

    public int getEsprit() {
        return esprit;
    }

    public void setEsprit(int esprit) {
        this.esprit = esprit;
    }

    public int getAgilite() {
        return agilite;
    }

    public void setAgilite(int agilite) {
        this.agilite = agilite;
    }

    public int getElement_feu() {
        return element_feu;
    }

    public void setElement_feu(int element_feu) {
        this.element_feu = element_feu;
    }

    public int getElement_air() {
        return element_air;
    }

    public void setElement_air(int element_air) {
        this.element_air = element_air;
    }

    public int getElement_foudre() {
        return element_foudre;
    }

    public void setElement_foudre(int element_foudre) {
        this.element_foudre = element_foudre;
    }

    public int getElement_lumiere() {
        return element_lumiere;
    }

    public void setElement_lumiere(int element_lumiere) {
        this.element_lumiere = element_lumiere;
    }

    public int getElement_eau() {
        return element_eau;
    }

    public void setElement_eau(int element_eau) {
        this.element_eau = element_eau;
    }

    public int getElement_terre() {
        return element_terre;
    }

    public void setElement_terre(int element_terre) {
        this.element_terre = element_terre;
    }

    public int getElement_nature() {
        return element_nature;
    }

    public void setElement_nature(int element_nature) {
        this.element_nature = element_nature;
    }

    public int getElement_tenebres() {
        return element_tenebres;
    }

    public void setElement_tenebres(int element_tenebres) {
        this.element_tenebres = element_tenebres;
    }

    public Weapon getArme() {
        return arme;
    }

    public void setArme(Weapon arme) {
        this.arme = arme;
    }

    public Protection getProtection() {
        return protection;
    }

    public void setProtection(Protection protection) {
        this.protection = protection;
    }

    public Accessory getAccessoire1() {
        return accessoire1;
    }

    public void setAccessoire1(Accessory accessoire1) {
        this.accessoire1 = accessoire1;
    }

    public Accessory getAccessoire2() {
        return accessoire2;
    }

    public void setAccessoire2(Accessory accessoire2) {
        this.accessoire2 = accessoire2;
    }
}
