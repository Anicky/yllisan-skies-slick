package fr.raversoft.yllisanSkies.data;

import fr.raversoft.yllisanSkies.data.enemies.Behavior;

/**
 * Enemy of the party in a battle.
 *
 * @author JALOUZET Jérémie
 */
public class Enemy {

    /**
     * Name of the enemy
     */
    private String name = "";
    /**
     * Description of the enemy
     */
    private String description = "";
    /**
     * Behavior of the enemy
     */
    private Behavior behavior = null;
    /**
     * Actual Health Points of the enemy
     */
    private int hp = 0;
    /**
     * Maximum Health Points of the enemy
     */
    private int hpMax = 0;
    /**
     * Actual Action Points of the enemy
     */
    private int ap = 0;
    /**
     * Maximum Action Points of the enemy
     */
    private int apMax = 0;
    /**
     * Strength of the enemy
     */
    private int strength = 0;
    /**
     * Resistance of the enemy
     */
    private int resistance = 0;
    /**
     * Potential of the enemy
     */
    private int potential = 0;
    /**
     * Spirit of the enemy
     */
    private int spirit = 0;
    /**
     * Agility of the enemy
     */
    private int agility = 0;
    /**
     * Affiliation with the Fire element
     */
    private int element_fire = 0;
    /**
     * Affiliation with the Air element
     */
    private int element_air = 0;
    /**
     * Affiliation with the Lightning element
     */
    private int element_lightning = 0;
    /**
     * Affiliation with the Light element
     */
    private int element_light = 0;
    /**
     * Affiliation with the Water element
     */
    private int element_water = 0;
    /**
     * Affiliation with the Earth element
     */
    private int element_earth = 0;
    /**
     * Affiliation with the Nature element
     */
    private int element_nature = 0;
    /**
     * Affiliation with the Darkness element
     */
    private int element_darkness = 0;

    /**
     * Associate a behavior to the enemy.
     *
     * @param behavior The behavior
     */
    public void setComportement(Behavior behavior) {
        this.behavior = behavior;
    }
}
