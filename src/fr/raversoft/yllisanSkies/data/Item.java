package fr.raversoft.yllisanSkies.data;

/**
 * Item owned by the party.
 *
 * @author JALOUZET Jérémie
 */
public abstract class Item {

    /**
     * Name of the item
     */
    protected String name = "";
    /**
     * Description of the item
     */
    protected String description = "";
    /**
     * Image (icon) of the item
     */
    protected String image = "";
}
