package fr.raversoft.yllisanSkies.data;

import fr.raversoft.yllisanSkies.exceptions.FullPartyException;
import java.util.ArrayList;

/**
 * Informations about the party of heroes.
 *
 * @author JALOUZET Jérémie
 */
public class Party {

    /**
     * Maximum number of heroes in the party.
     */
    public final static int MAX_SIZE = 4;
    /**
     * Inventory of the party (all the items owned by the party).
     */
    private ArrayList<Item> inventory = null;
    /**
     * Heroes who are actually in the party.
     */
    private ArrayList<Hero> heroes = null;
    /**
     * Money owned by the party.
     */
    private int money = 0;

    /**
     * Create a party.
     */
    public Party() {
        inventory = new ArrayList<>();
        heroes = new ArrayList<>();
    }

    /**
     * Get the number of heroes in the party.
     *
     * @return The number of heroes in the party
     */
    public int getSize() {
        return heroes.size();
    }

    /**
     * Add a hero to the party.
     *
     * @param hero The hero to be added to the party
     * @throws PartyCompleteException
     */
    public void addHero(Hero hero) throws FullPartyException {
        if (getSize() < MAX_SIZE) {
            heroes.add(hero);
        } else {
            throw new FullPartyException(Parameters.getText("Exceptions", "FullParty"));
        }
    }

    /**
     * Récupère le personnage leader de l'équipe (celui en 1ère position)
     *
     * @return Un personnage
     */
    public Hero getPersonnageLeader() {
        return heroes.get(0);
    }

    /**
     * Récupère le personnage à une position donnée
     *
     * @param position La position du personnage à récupérer
     * @return Un personnage
     */
    public Hero getPersonnage(int position) {
        return heroes.get(position - 1);
    }

    /**
     * Supprime un personnage de l'équipe
     *
     * @param personnage Le personnage à supprimer
     * @return Un booléen indiquant si le personnage a été trouvé
     */
    public boolean removePersonnage(Hero personnage) {
        return heroes.remove(personnage);
    }

    /**
     * Supprime un personnage de l'équipe à une position donée
     *
     * @param position La position du personnage à supprimer de l'équipe
     * @return Un personnage
     */
    public Hero removePersonnage(int position) {
        return heroes.remove(position - 1);
    }

    public int getArgent() {
        return money;
    }
}
