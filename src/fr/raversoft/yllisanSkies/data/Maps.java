package fr.raversoft.yllisanSkies.data;

import fr.raversoft.yllisanSkies.engine.Map;
import fr.raversoft.yllisanSkies.engine.Resources;
import java.util.ArrayList;
import java.util.List;
import org.newdawn.slick.SlickException;

/**
 *
 * @author JALOUZET Jérémie
 */
public class Maps {

    private List<String> maps = new ArrayList<>();
    public final static int FORET_DES_ESPOIRS_1 = 0;
    public final static int TEST = 1;

    public Maps() throws SlickException {
        int i = 0;
        maps.add(i++, Resources.getMap("essai_01"));
        maps.add(i++, Resources.getMap("essai_02"));
    }
    
    public Map getMap(int map) throws SlickException {
        return new Map(maps.get(map));
    }
}
