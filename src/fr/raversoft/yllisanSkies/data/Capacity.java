package fr.raversoft.yllisanSkies.data;

/**
 * Capacity of a hero or an enemy
 *
 * @author JALOUZET Jérémie
 */
public abstract class Capacity {

    /**
     * Name of the capacity
     */
    private String name = "";
    /**
     * Description of the capacity
     */
    private String description = "";
    /**
     * Cost (in Capacity Points) needed to equip the capacity
     */
    private int cost_cp = 0;
    /**
     * Cost (in Action Points) needed to cast the capacity
     */
    private int cost_ap = 0;
    /**
     * Level of the capacity's mastery by the hero/enemy
     */
    private int mastery_level = 0;
    /**
     * Points of mastery needed to improve a level the capacity's mastery
     */
    private int mastery_ratio = 0;

    /**
     * Create a capacity.
     *
     * @param name Name of the capacity
     * @param description Description of the capacity
     * @param cost_cp Cost in Capacity Points
     * @param cost_ap Cost in Action Points
     * @param mastery_ratio Ratio needed for mastering a level of the capacity
     */
    public Capacity(String name, String description, int cost_cp, int cost_ap, int mastery_ratio) {
        this.name = name;
        this.description = description;
        this.cost_cp = cost_cp;
        this.cost_ap = cost_ap;
        this.mastery_ratio = mastery_ratio;
    }
}
