package fr.raversoft.yllisanSkies.data;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author JALOUZET Jérémie
 */
public class Parameters {

    private static Locale language = null;

    public static String getText(String type, String key) {
        if (language == null) {
            language = Locale.getDefault();
        }
        return ResourceBundle.getBundle("fr.raversoft.yllisanSkies.res.texts." + type, language).getString(key);
    }
}
