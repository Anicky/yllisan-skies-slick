package fr.raversoft.yllisanSkies.data.capacities;

import fr.raversoft.yllisanSkies.data.Capacity;

/**
 * Capacity based on Potential / Spirit and Elements
 *
 * @author JALOUZET Jérémie
 */
public class Magic extends Capacity {

    /**
     * Create a magic.
     *
     * @param name Name of the capacity
     * @param description Description of the capacity
     * @param cost_cp Cost in Capacity Points
     * @param cost_ap Cost in Action Points
     * @param mastery_ratio Ratio needed for mastering a level of the capacity
     */
    public Magic(String name, String description, int cost_cp, int cost_ap, int mastery_ratio) {
        super(name, description, cost_cp, cost_ap, mastery_ratio);
    }
}
