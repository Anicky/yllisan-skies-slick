package fr.raversoft.yllisanSkies.data.capacities;

import fr.raversoft.yllisanSkies.data.Capacity;

/**
 * Capacity which don't need to be casted in a battle ; these effects are active
 * if the capacity is equipped.
 *
 * @author JALOUZET Jérémie
 */
public class Passive extends Capacity {

    /**
     * Create a passive capacity.
     *
     * @param name Name of the capacity
     * @param description Description of the capacity
     * @param cost_cp Cost in Capacity Points
     * @param cost_ap Cost in Action Points
     * @param mastery_ratio Ratio needed for mastering a level of the capacity
     */
    public Passive(String name, String description, int cost_cp, int cost_ap, int mastery_ratio) {
        super(name, description, cost_cp, cost_ap, mastery_ratio);
    }
}