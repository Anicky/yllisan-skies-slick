package fr.raversoft.yllisanSkies.data.items;

import fr.raversoft.yllisanSkies.data.Item;

/**
 * Item which can improve permanently some stats of a hero.
 *
 * @author JALOUZET Jérémie
 */
public class PermanentItem extends Item {
}
