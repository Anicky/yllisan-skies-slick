package fr.raversoft.yllisanSkies.data.items;

import fr.raversoft.yllisanSkies.data.Item;

/**
 * Various items used for quests and the story (a key, a letter, ...)
 *
 * @author JALOUZET Jérémie
 */
public class MiscellaneousItem extends Item {
}
