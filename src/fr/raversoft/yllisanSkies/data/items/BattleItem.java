package fr.raversoft.yllisanSkies.data.items;

import fr.raversoft.yllisanSkies.data.Item;

/**
 * Item usable in battles only.
 *
 * @author JALOUZET Jérémie
 */
public class BattleItem extends Item {
}
