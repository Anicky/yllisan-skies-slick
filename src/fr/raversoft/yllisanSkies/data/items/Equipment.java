package fr.raversoft.yllisanSkies.data.items;

import fr.raversoft.yllisanSkies.data.Item;

/**
 * Item equipped by a hero : improve its stats and give sometimes a capacity.
 *
 * @author JALOUZET Jérémie
 */
public abstract class Equipment extends Item {

    protected int strength = 0;
    protected int resistance = 0;
    protected int potential = 0;
    protected int spirit = 0;
    protected int agility = 0;
}
