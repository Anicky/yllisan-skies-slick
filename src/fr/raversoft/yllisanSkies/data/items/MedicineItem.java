package fr.raversoft.yllisanSkies.data.items;

import fr.raversoft.yllisanSkies.data.Item;

/**
 * Item used for regaining Hp / Ap of a hero (usable in the menu and in
 * battles).
 *
 * @author JALOUZET Jérémie
 */
public class MedicineItem extends Item {
}
