package fr.raversoft.yllisanSkies.data.items.equipment;

import fr.raversoft.yllisanSkies.data.items.Equipment;

/**
 * Specific to a hero : improve offensive stats (Strength, Potential) and give
 * sometimes a capacity (Skill or Magic)
 *
 * @author JALOUZET Jérémie
 */
public class Weapon extends Equipment {
}
