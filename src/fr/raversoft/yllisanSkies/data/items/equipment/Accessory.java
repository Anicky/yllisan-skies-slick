package fr.raversoft.yllisanSkies.data.items.equipment;

import fr.raversoft.yllisanSkies.data.items.Equipment;

/**
 * Can be equipped by every hero : improve some stats, and give sometimes a
 * passive capacity.
 *
 * @author JALOUZET Jérémie
 */
public class Accessory extends Equipment {
}
