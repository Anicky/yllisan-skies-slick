package fr.raversoft.yllisanSkies.data.items.equipment;

import fr.raversoft.yllisanSkies.data.items.Equipment;

/**
 * Can be equipped most of the time by every hero : improve defensive stats
 * (Resistance, Spirit)
 *
 * @author JALOUZET Jérémie
 */
public class Protection extends Equipment {
}
