package fr.raversoft.yllisanSkies.data;

/**
 * Element used for capacities, equipment, ... Every elements has its opposite :
 * Fire / Water, Air / Earth, Lightning / Nature, Light / Darkness
 *
 * @author JALOUZET Jérémie
 */
public enum Element {

    Fire, Water, Air, Earth, Lightning, Nature, Light, Darkness
}
