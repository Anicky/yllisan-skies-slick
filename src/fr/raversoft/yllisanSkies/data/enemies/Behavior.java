package fr.raversoft.yllisanSkies.data.enemies;

/**
 * Behavior of an enemy (determines how it behaves in battles).
 *
 * @author JALOUZET Jérémie
 */
public interface Behavior {
}
