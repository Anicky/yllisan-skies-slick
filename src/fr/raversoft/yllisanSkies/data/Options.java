package fr.raversoft.yllisanSkies.data;

/**
 *
 * @author JALOUZET Jérémie
 */
public class Options {

    private static boolean JEU_EFFETS_LUMIERE = true;
    private static int JEU_VITESSE_DIALOGUES = 0;
    private static boolean JEU_ALLIES_SUIVENT = true;
    private static boolean JEU_ATTRIBUTION_POINTS_AUTO = true;
    private static boolean MENU_EFFETS = true;
    private static boolean MENU_MEMO_CURSEUR = true;
    private static boolean COMBATS_EFFETS = true;
    private static boolean COMBATS_MEMO_CURSEUR = true;
    private static int COMBATS_VITESSE = 0;
    private static int COMBATS_TRANSITION = 0;
    private static int COMBATS_MUSIQUE = 0;
    private static int COMBATS_POINTS_ATTAQUE = 0;

    public static boolean getEffetsLumiere() {
        return JEU_EFFETS_LUMIERE;
    }
}
