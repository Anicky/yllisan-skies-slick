package fr.raversoft.yllisanSkies.data;

import fr.raversoft.yllisanSkies.engine.Resources;
import java.util.ArrayList;
import java.util.List;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/**
 *
 * @author JALOUZET Jérémie
 */
public class Sounds {

    private List<Sound> sons = new ArrayList<>();
    public final static int CANCEL = 0;
    public final static int BUZZER = 1;
    public final static int CURSOR = 2;
    public final static int OK = 3;
    private float volume = 1.0f;

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public Sounds() throws SlickException {
        int i = 0;
        sons.add(i++, Resources.getSound("cancel"));
        sons.add(i++, Resources.getSound("buzzer"));
        sons.add(i++, Resources.getSound("cursor"));
        sons.add(i++, Resources.getSound("ok"));
    }

    public void play(int music) {
        play(music, 1.0f, 1.0f);
    }

    public void play(int music, float volume) {
        play(music, 1.0f, volume);
    }

    public void play(int music, float pitch, float volume) {
        sons.get(music).play(pitch, (this.volume) * volume);
    }
}
