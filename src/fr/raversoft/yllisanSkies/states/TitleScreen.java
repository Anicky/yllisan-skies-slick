package fr.raversoft.yllisanSkies.states;

import fr.raversoft.yllisanSkies.engine.Commands;
import fr.raversoft.yllisanSkies.data.Sounds;
import fr.raversoft.yllisanSkies.engine.ImageMovement;
import fr.raversoft.yllisanSkies.engine.Police;
import fr.raversoft.yllisanSkies.engine.Resources;
import fr.raversoft.yllisanSkies.MainGame;
import fr.raversoft.yllisanSkies.data.MusicList;
import fr.raversoft.yllisanSkies.data.Parameters;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

/**
 *
 * @author JALOUZET Jeremie
 */
public class TitleScreen extends BasicGameState {

    private boolean loaded = false;
    private int selection = 1;
    private int selection_before;
    private boolean cursor_movement = false;
    private MainGame mainGame = null;
    private ImageMovement movement = null;

    public TitleScreen(MainGame mainGame) {
        this.mainGame = mainGame;
    }

    @Override
    public int getID() {
        return 0;
    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
    }

    private void load() {
        mainGame.music.play(MusicList.TITLE_SCREEN, 0.5f);
        loaded = true;
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        if (!loaded) {
            load();
        }
        // Panorama
        g.drawImage(Resources.getPanorama("TitleScreen"), 0, 0);
        final int base_x = 400, base_y = 268;
        float item_x, item_y, text_x, text_y;
        String text = "";
        for (int i = 1; i <= 4; i++) {
            switch (i) {
                case 1:
                    text = Parameters.getText("TitleScreen", "NewGame");
                    break;
                case 2:
                    text = Parameters.getText("TitleScreen", "Continue");
                    break;
                case 3:
                    text = Parameters.getText("TitleScreen", "Bonus");
                    break;
                case 4:
                    text = Parameters.getText("TitleScreen", "Quit");
                    break;
            }

            float k = 0;
            if (cursor_movement) {
                if (i == selection) {
                    k = movement.getPosition();
                } else if (i == selection_before) {
                    k = mainGame.SCREEN_TILE - movement.getPosition();
                }
            } else if (selection == i) {
                k = mainGame.SCREEN_TILE;
            }

            item_x = base_x - k;
            if (i == 2 | i == 3) {
                item_x -= (mainGame.SCREEN_TILE / 4);
            }
            item_y = base_y + (i * mainGame.SCREEN_TILE);
            text_x = item_x + (mainGame.SCREEN_TILE / 2);
            text_y = item_y + (mainGame.SCREEN_TILE / 8);
            g.drawImage(Resources.getPicture("item"), item_x, item_y);
            Police.getPolice(Police.STANDARD).drawString(text_x, text_y, text);
        }
        if (cursor_movement) {
            movement.next();
            if (movement.isOver()) {
                cursor_movement = false;
            }
        }
    }

    private void cursorMove() {
        cursor_movement = true;
        movement = new ImageMovement(12, mainGame.SCREEN_TILE);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        if (!cursor_movement) {
            if (container.getInput().isKeyDown(Commands.up())) {
                mainGame.playSound(Sounds.CURSOR);
                selection_before = selection;
                selection--;
                if (selection < 1) {
                    selection = 4;
                }
                cursorMove();
            }
            if (container.getInput().isKeyDown(Commands.down())) {
                mainGame.playSound(Sounds.CURSOR);
                selection_before = selection;
                selection++;
                if (selection > 4) {
                    selection = 1;
                }
                cursorMove();
            }
        }
        if (container.getInput().isKeyPressed(Input.KEY_ENTER)) {
            mainGame.playSound(Sounds.OK);
            switch (selection) {
                case 1:
                    mainGame.music.fade(1000);
                    game.enterState(MainGame.EXPLORATION, new FadeOutTransition(Color.black, 1000), new FadeInTransition(Color.black, 1000));
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    mainGame.music.fade(1000);
                    mainGame.quit(container);
                    break;
            }
        }
    }
}
