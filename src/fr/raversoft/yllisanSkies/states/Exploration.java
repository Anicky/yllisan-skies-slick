package fr.raversoft.yllisanSkies.states;

import static fr.raversoft.yllisanSkies.tests.Jeu.CASE;
import static fr.raversoft.yllisanSkies.tests.Jeu.ECRAN_LARGEUR;
import fr.raversoft.yllisanSkies.data.Maps;
import fr.raversoft.yllisanSkies.data.MusicList;
import fr.raversoft.yllisanSkies.data.Options;
import fr.raversoft.yllisanSkies.engine.Camera;
import fr.raversoft.yllisanSkies.engine.Direction;
import fr.raversoft.yllisanSkies.engine.Police;
import fr.raversoft.yllisanSkies.engine.Resources;
import fr.raversoft.yllisanSkies.MainGame;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author Jeremie
 */
public class Exploration extends BasicGameState {

    private MainGame mainGame = null;
    private boolean loaded = false;

    public Exploration(MainGame mainGame) {
        this.mainGame = mainGame;
    }

    @Override
    public int getID() {
        return 1;
    }

    private void load() throws SlickException, IOException {
        mainGame.map_actual = mainGame.maps.getMap(Maps.FORET_DES_ESPOIRS_1);
        mainGame.camera = new Camera(mainGame, mainGame.screen, mainGame.map_actual, 2 * 32, 20 * 32, Direction.Droite);
        mainGame.music.play(MusicList.HOPE_FOREST, 0.5f);
        loaded = true;
    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
    }

    private void gestionClavier(GameContainer container) throws SlickException {
        // Touche F4 (Fullscreen)
        if (container.getInput().isKeyPressed(Input.KEY_F4)) {
            switchSetFullscreen();
        }

        // Touche F5 (Fantome)
        if (container.getInput().isKeyPressed(Input.KEY_F5)) {
            switchModeFantome();
        }

        // Touche F6 (Debug)
        if (container.getInput().isKeyPressed(Input.KEY_F6)) {
            switchModeDebug();
        }
    }

    private void switchModeDebug() {
        if (mainGame.screen.estModeDebug()) {
            mainGame.screen.setModeDebug(false);
            mainGame.getContainer().setShowFPS(false);
        } else {
            mainGame.screen.setModeDebug(true);
            mainGame.getContainer().setShowFPS(true);
        }
    }

    private void switchModeFantome() {
        if (mainGame.screen.estModeFantome()) {
            mainGame.screen.setModeFantome(false);
        } else {
            mainGame.screen.setModeFantome(true);
        }
    }

    private void switchSetFullscreen() throws SlickException {
        if (mainGame.screen.estPleinEcran()) {
            mainGame.screen.setPleinEcran(false);
        } else {
            mainGame.screen.setPleinEcran(true);
        }
        mainGame.getContainer().setFullscreen(mainGame.screen.estPleinEcran());
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        if (!loaded) {
            try {
                load();
            } catch (IOException ex) {
                Logger.getLogger(Exploration.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        mainGame.camera.affiche(g);
        if (Options.getEffetsLumiere()) {
            g.drawImage(Resources.getPicture("Effet_Soleil_03"), 0, 0, new Color(1f, 1f, 1f, 0.3f));
        }
        if (mainGame.screen.estModeDebug()) {
            afficheDebug(g);
        }
        if (mainGame.screen.estModeFantome()) {
            afficheModeFantome();
        }
    }

    private void afficheModeFantome() throws SlickException {
        Police.getPolice(Police.STANDARD).drawString(mainGame.SCREEN_TILE / 4, 14 * mainGame.SCREEN_TILE, "Mode fantôme");
    }

    private void afficheDebug(Graphics g) throws SlickException {
        afficheGrille(g);
        for (int i = 0; i < mainGame.map_actual.getNombreBlocs(); i++) {
            mainGame.map_actual.getBloc(i).afficheForme(g);
        }
        g.setColor(org.newdawn.slick.Color.red);
        g.draw(mainGame.camera.getPersoContour());
        afficheInfosPositions();
    }

    private void afficheInfosPositions() throws SlickException {
        String persoEcran = "Perso.Ecran X " + (mainGame.camera.getPersoEcranX() + CASE / 4) + " Y " + (mainGame.camera.getPersoEcranY() + CASE);
        String persoMapEcran = "Perso.Map X " + mainGame.camera.getPersoMapX() + " Y " + mainGame.camera.getPersoMapY();
        String mapEcran = "Map.Ecran X " + mainGame.map_actual.x + " Y " + mainGame.map_actual.y;
        Police.getPolice(Police.STANDARD).drawString(ECRAN_LARGEUR - Police.getPolice(Police.STANDARD).getWidth(persoEcran) - (CASE / 4), CASE / 4, persoEcran);
        Police.getPolice(Police.STANDARD).drawString(ECRAN_LARGEUR - Police.getPolice(Police.STANDARD).getWidth(persoMapEcran) - (CASE / 4), CASE / 4 + 1 * CASE, persoMapEcran);
        Police.getPolice(Police.STANDARD).drawString(ECRAN_LARGEUR - Police.getPolice(Police.STANDARD).getWidth(mapEcran) - (CASE / 4), CASE / 4 + 2 * CASE, mapEcran);
    }

    private void afficheGrille(Graphics g) throws SlickException {
        int x = mainGame.map_actual.x % mainGame.SCREEN_TILE;
        int y = mainGame.map_actual.y % mainGame.SCREEN_TILE;
        g.drawImage(Resources.getPicture("debug_grille"), x, y);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        mainGame.time.tick();
        gestionClavier(container);
        if (!mainGame.camera.estEnMouvement()) {
            mainGame.camera.gestionClavier(container);
        } else {
            mainGame.camera.deplacement(mainGame.camera.getDirection());
        }
    }
}
