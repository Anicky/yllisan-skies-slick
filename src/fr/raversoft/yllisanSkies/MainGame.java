package fr.raversoft.yllisanSkies;

import fr.raversoft.yllisanSkies.states.Exploration;
import fr.raversoft.yllisanSkies.states.TitleScreen;
import static fr.raversoft.yllisanSkies.tests.Jeu.ECRAN_DEBUG;
import static fr.raversoft.yllisanSkies.tests.Jeu.ECRAN_HAUTEUR;
import static fr.raversoft.yllisanSkies.tests.Jeu.ECRAN_LARGEUR;
import static fr.raversoft.yllisanSkies.tests.Jeu.ECRAN_PLEIN_ECRAN;
import fr.raversoft.yllisanSkies.data.Hero;
import fr.raversoft.yllisanSkies.data.Maps;
import fr.raversoft.yllisanSkies.data.MusicList;
import fr.raversoft.yllisanSkies.data.Party;
import fr.raversoft.yllisanSkies.data.Sounds;
import fr.raversoft.yllisanSkies.engine.Camera;
import fr.raversoft.yllisanSkies.engine.Map;
import fr.raversoft.yllisanSkies.engine.Police;
import fr.raversoft.yllisanSkies.engine.Screen;
import fr.raversoft.yllisanSkies.engine.Temps;
import fr.raversoft.yllisanSkies.exceptions.FullPartyException;
import java.io.IOException;
import org.lwjgl.openal.AL;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class MainGame extends StateBasedGame {

    // Miscellaneous
    public final static String gameName = "Yllisan Skies";
    // States
    public static final int TITLE_SCREEN = 0;
    public static final int EXPLORATION = 1;
    public static final int MENU = 2;
    public static final int BATTLE = 3;
    // Components
    public Camera camera = null;
    public Maps maps = null;
    public Screen screen;
    private Party party = null;
    public Temps time = null;
    private Sounds sounds = null;
    public MusicList music = null;
    public Map map_actual = null;
    // Screen
    public final int SCREEN_WIDTH = 640;
    public final int SCREEN_HEIGHT = 480;
    public final int SCREEN_TILE = 32;
    public final int SCREEN_FPS = 60;
    // Modes
    public final boolean MODE_FULLSCREEN = false;
    public final boolean MODE_DEBUG = false;
    public final boolean MODE_FPS = false;

    public void playSound(int sound) {
        sounds.play(sound);
    }

    public Party getParty() {
        return party;
    }

    public MainGame(String name) throws SlickException, IOException, FullPartyException {
        super(name);
        party = new Party();
        screen = new Screen(ECRAN_LARGEUR, ECRAN_HAUTEUR, ECRAN_PLEIN_ECRAN, ECRAN_DEBUG);
        time = new Temps();
        sounds = new Sounds();
        music = new MusicList();
        Hero.creerHeros();
        party.addHero(Hero.getHeros("Cyril"));
        party.addHero(Hero.getHeros("Max"));
        party.addHero(Hero.getHeros("Yuna"));
        party.addHero(Hero.getHeros("Leonard"));
        maps = new Maps();
    }

    public static void main(String[] args) {
        AppGameContainer app;
        try {
            MainGame game = new MainGame(gameName);
            app = new AppGameContainer(game);
            app.setDisplayMode(game.SCREEN_WIDTH, game.SCREEN_HEIGHT, game.MODE_FULLSCREEN);
            app.setShowFPS(game.MODE_FPS);
            app.setTargetFrameRate(game.SCREEN_FPS);
            app.setVSync(true);
            app.start();
            app.setDefaultFont(Police.getPolice(Police.STANDARD));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initStatesList(GameContainer container) throws SlickException {
        this.addState(new TitleScreen(this));
        this.addState(new Exploration(this));
        this.enterState(TITLE_SCREEN);
    }

    public void quit(GameContainer container) {
        AL.destroy();
        container.destroy();
        System.exit(0);
    }
}
