package fr.raversoft.yllisanSkies.exceptions;

/**
 * Exception throw when we try to add a hero to the party, while it's already
 * full.
 *
 * @author JALOUZET Jérémie
 */
public class FullPartyException extends Exception {

    public FullPartyException(String message) {
        super(message);
    }
}
